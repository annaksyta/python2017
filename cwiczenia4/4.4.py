#!/usr/bin/python
# Anna Ksyta
#
# Program liczby n-ty wyraz ciagu Fibonacciego pobranego z argumentu

import sys

def fibonacci(n):
	result = 0
	y = 1
	x = 1
	for i in range(n+1):
		result = y
		y = x
		x = result + y
	return result

if len(sys.argv) < 2:
	print 'Wywolanie programu python 4.4.py n'
else:
	try:
		print 'fibonacci(',sys.argv[1],') = ', fibonacci(int(sys.argv[1]))
	except:
		print 'Podany argument nie jest liczba'	 