#!/usr/bin/python
# Anna Ksyta
#
# Program liczy silnie liczby pobranej z argumentu wywolania

import sys

def factorial(n):
	result = 1
	for i in range(1, n+1):
		result *= i
	return result

if len(sys.argv) < 2:
	print 'Wywolanie programu python 4.3.py n'
else:
	try:
		print 'Factorial(',sys.argv[1],') = ', factorial(int(sys.argv[1]))
	except:
		print 'Podany argument nie jest liczba'