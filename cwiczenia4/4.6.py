#!/usr/bin/python
# Anna Ksyta

def sum_seq(sequence):
	result = 0
	for i in sequence:
		if isinstance(i, (list, tuple)):
			result += sum_seq(i)
		else:
			result += i
	return result

lista = [1, 2, [42, 2], (32, 21), 5]
print sum_seq(lista)