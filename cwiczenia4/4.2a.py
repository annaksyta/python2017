#!/usr/bin/python
# Anna Ksyta
#
# Program wypisujacy miarke o zadanej dlugosci korzystajacy z funkcji

import sys

def getMeasure(max_length):
	max_digits = len(str(max_length))
	output = ''
	for i in range(0, max_length):
		output += '|'
		for j in range(0, max_digits+1):
			output += '.'
	output += '|\n'
	for i in range(0, max_length+1):
		output += str(i)
		for i in range(len(str(i+1))-1, max_digits+1):
			output += ' '
	return output

if len(sys.argv) != 2:
	print 'Wywolanie programu:', __file__, ' maksymalna_dlugosc_miarki'
	sys.exit()
else:
	try:
		max_length = int(sys.argv[1])
		if max_length < 1:
			raise ValueError('Podana wartosc nie jest liczba naturalna wieksza od 0')
	except ValueError:
		print 'Podany argument musi byc liczba naturalna'
		sys.exit()

print getMeasure(max_length)