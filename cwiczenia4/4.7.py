#!/usr/bin/python
# Anna Ksyta

def flatten(sequence):
	result = []
	for i in sequence:
		if isinstance(i, (list, tuple)):
			sequence.extend(i)
		else:
			result.append(i)
	return result
			
seq = [1,(2,3),[],[4,(5,6,7,[4,42])],8,[9]]
print flatten(seq)