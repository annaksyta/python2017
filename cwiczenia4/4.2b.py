#!/usr/bin/python
# Anna Ksyta
#
# Program wyswietlajacy tabele o zadanej ilosci kolumn i wierszy

import sys

def getTable(rows, columns):
	rowsDivider = ('---').join("+" * (columns+1))
	rowsContent = ('   ').join("|" * (columns+1))

	output = ''
	for i in range(0, rows):
		output += rowsDivider + '\n' + rowsContent + '\n'
	output += rowsDivider
	return output

if len(sys.argv) != 3:
	print 'Wywolanie programu:', __file__, 'ilosc_wierszy ilosc_kolumna'
	sys.exit()
else:
	try:
		rows = int(sys.argv[1])
		columns = int(sys.argv[2])
		if columns < 1 or rows < 1:
			raise ValueError('Jedna z podanych wartosci nie jest liczba naturalna wieksza od 0')
	except ValueError:
		print 'Podany argument musi byc liczba naturalna'
		sys.exit()

print getTable(rows, columns)