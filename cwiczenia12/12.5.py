def moda_py(L, left, right):
	D = dict()
	for i in range(left, right+1):
		D[L[i]] = D.get(L[i], 0) + 1
	item = max(D, key=D.get)
	if D[item] > 1:
		for i in range(left, right+1):
			if L[i] == item:
				return item
	else:
		return None


print moda_py([1, 2, 3, 3, 1, 2, 1, 1], 0, 7)