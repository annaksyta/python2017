import unittest
from points import *
import math

class TestPoint(unittest.TestCase):

	def setUp(self):
		self.point = Point(1, 2)

	def test_print(self): # test str() i repr()
		self.assertEqual(self.point.__repr__(), "Point(1, 2)")
		self.assertEqual(self.point.__str__(), "(1, 2)")

	def test_eq_ne(self):	# test ==, !=
		self.assertTrue(self.point ==  Point(1, 2))
		self.assertTrue(self.point != Point(1, 3))

	def test_add(self):
		self.assertEqual(self.point + self.point, Point(2, 4))

	def test_sub(self):
		self.assertEqual(self.point - self.point, Point(0, 0))
		self.assertEqual(self.point - Point(-1, 3), Point(2, -1))

	def test_mul(self):
		self.assertEqual(self.point * self.point, 5)

	def test_cross(self):
		self.assertEqual(self.point.cross(self.point), 0)

	def test_length(self):
		self.assertEqual(self.point.length(), math.sqrt(5))

	def tearDown(self):
		pass

if __name__ == "__main__":
	unittest.main()