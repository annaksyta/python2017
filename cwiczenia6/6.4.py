import unittest
from triangles import *

class TestTriangle(unittest.TestCase):
	def setUp(self):
		self.triangle = Triangle(0, 0, 2, 0, 1, 2)

	def test_print(self):
		self.assertEqual(self.triangle.__repr__(), "Triangle(0, 0, 2, 0, 1, 2)")
		self.assertEqual(self.triangle.__str__(), "[(0, 0), (2, 0), (1, 2)]")

	def test_eq_ne(self):
		self.assertTrue(self.triangle == Triangle(0, 0, 2, 0, 1, 2))
		self.assertTrue(self.triangle != Triangle(0, 0, 1, 1, 0, 1))

	def test_center(self):
		self.assertEqual(self.triangle.center(), Point(1, 2/3))

	def test_area(self):
		self.assertEqual(self.triangle.area(), 2)

	def test_move(self):
		self.assertEqual(self.triangle.move(1, 1), Triangle(1, 1, 3, 1, 2, 3))
	
	def tearDown(self):
		pass

if __name__ == "__main__":
	unittest.main()