from points import Point
import math

class Triangle:
    """Klasa reprezentujaca trojkat na plaszczyznie."""

    def __init__(self, x1=0, y1=0, x2=0, y2=0, x3=0, y3=0):
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)
        self.pt3 = Point(x3, y3)

    def __str__(self):         # "[(x1, y1), (x2, y2), (x3, y3)]"
        return "[%s, %s, %s]" % (self.pt1, self.pt2, self.pt3)

    def __repr__(self):       # "Triangle(x1, y1, x2, y2, x3, y3)"
        return "Triangle(%d, %d, %d, %d, %d, %d)" % (self.pt1.x, self.pt1.y, self.pt2.x, self.pt2.y, self.pt3.x, self.pt3.y)

    def __eq__(self, other):   # obsluga tr1 == tr2
        return self.pt1 == other.pt1 and self.pt2 == other.pt2 and self.pt3 == other.pt3

    def __ne__(self, other):        # obsluga tr1 != tr2
        return not self == other

    def center(self):          # zwraca srodek trojkata
        return Point((self.pt1.x + self.pt2.x + self.pt3.x) / 3, (self.pt1.y + self.pt2.y + self.pt3.y) / 3)

    def area(self):            # pole powierzchni
        a = math.sqrt(pow(self.pt2.x - self.pt1.x, 2) + pow(self.pt2.y - self.pt1.y, 2))
        b = math.sqrt(pow(self.pt3.x - self.pt2.x, 2) + pow(self.pt3.y - self.pt2.y, 2))
        c = math.sqrt(pow(self.pt1.x - self.pt3.x, 2) + pow(self.pt1.y - self.pt3.y, 2))
        O = a + b + c
        p = O / 2
        P = math.sqrt(p * (p - a) * (p - b) * (p - c))
        return P

    def move(self, x, y):      # przesuniecie o (x, y)
        return Triangle(self.pt1.x + x, self.pt1.y + y, self.pt2.x + x, self.pt2.y + y, self.pt3.x + x, self.pt3.y + y)