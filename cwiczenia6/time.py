class Time:
    """Klasa reprezentujaca odcinek czasu."""

    def __init__(self, s=0):
        """Zwraca instancje klasy Time."""
        self.s = int(s)

    def __str__(self):
        """Zwraca string 'hh:mm:ss'."""
        h = self.s / 3600
        sec = self.s - h * 3600
        m = sec / 60
        sec = sec - m * 60
        return "%s:%s:%s" % (str(h).zfill(2),
            str(m).zfill(2), str(sec).zfill(2))

    def __repr__(self):
        """Zwraca string 'Time(s)'."""
        return "Time(%s)" % self.s

    def __add__(self, other):
        """Dodawanie odcinkow czasu."""
        return Time(self.s + other.s)

    def __cmp__(self, other):           # porownywanie, -1|0|+1
        """Porownywanie odcinkow czasu."""
        return cmp(self.s, other.s)

    def __int__(self):                  # int(time1)
        """Konwersja odcinka czasu do int."""
        return self.s