import unittest
from times import *

class TestTime(unittest.TestCase):
    def setUp(self):
        self.time = Time(3665) # 1h 1min 5s

    def test_print(self):      # test str() i repr()
        self.assertEqual(self.time.__repr__(), "Time(3665)")
        self.assertEqual(self.time.__str__(), "01:01:05")

    def test_add(self):
        self.assertEqual(Time(1) + Time(2), Time(3))

    def test_cmp(self):
        # Mozna sprawdzac ==, !=, >, >=, <, <=.
        self.assertTrue(Time(1) == Time(1))
        self.assertTrue(Time(1) != Time(2))
        self.assertTrue(Time(3) > Time(2))

    def test_int(self):
        self.assertEqual(self.time.__int__(), 3665)

    def tearDown(self): pass

if __name__ == "__main__":
    unittest.main()     # wszystkie testy