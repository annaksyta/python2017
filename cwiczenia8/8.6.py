def recursionP(i, j):
	if i == 0 and j == 0:
		return 0.5
	elif i > 0 and j == 0:
		return 0
	elif i == 0 and j > 0:
		return 1
	elif i > 0 and j > 0:
		return 0.5 * (recursionP(i-1, j) + recursionP(i, j-1))

def dynamicP(i, j):
	p = {"00": 0.5, "n0": 0, "0n": 1}
	if i == 0 and j == 0:
		return p["00"]
	elif i > 0 and j == 0:
		return p["n0"]
	elif i == 0 and j > 0:
		return p["0n"]
	elif i > 0 and j > 0:
		p[str(i) + str(j)] = 0.5 * (dynamicP(i-1, j) + dynamicP(i, j-1))
		return p[str(i) + str(j)]

if __name__ == "__main__":
	print recursionP(5,5)
	print dynamicP(5,5)