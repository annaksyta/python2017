import random

def calc_pi(n=100):
	"""Obliczanie liczby pi metoda Monte Carlo.
	n oznacza liczbe losowanych punktow."""
	repeats = n
	count = 0
	for _ in range(repeats):
		x, y = random.random(), random.random()
		if (((0.5 - x) ** 2) + ((0.5 - y) ** 2)) <= 0.25:
			count+=1

	result = 4 * count / float(n)
	return result

if __name__ == '__main__':
	print calc_pi(999999)