from math import sqrt
def heron(a, b, c):
	if (a + b) > c and (a + c) > b and (b + c) > a:
		p = (a + b + c) / float(2)
		return sqrt(p * (p - a) * (p - b) * (p - c))
	else:
		raise ValueError('Z podanych parametrow nie mozna utworzyc trojkata')

if __name__ == '__main__':
	print heron(2, 2, 3)