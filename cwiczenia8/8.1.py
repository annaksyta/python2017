def solve1(a, b, c):
	""" Rozwiazywanie rownania liniowego a x + b y + c = 0. """
	print "Rownanie: %gx + %gy + %g = 0" % (a, b, c) ,
	if a == 0 and b == 0:
		if not c == 0:
			print "jest rownianiem sprzecznym"
		else:
			print "jest rownianiem tozsamosciowym"
	else:
		if a == 0 and b != 0:
			y = -c / b
			print ", rozwiazanie rownania: y = %g" % y
		elif a != 0 and b == 0:
			x = -c / a
			print ", rozwiazanie rownania: x = %g" % x
		else:
			print ", rozwiazanie rownania: x, y = %gx + (%g), a x nalezy do liczb rzeczywistych" % (float(-a/b), float(-c/b))


if __name__ == '__main__':
	solve1(1, 2, 3)
	solve1(0, 0, 0)
	solve1(0, 0, 2)
	solve1(0, 1, 2)
	solve1(1, 0, 2)