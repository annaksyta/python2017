import fractions

class Frac:
    """Klasa reprezentujaca ulamki."""

    def __init__(self, x=0, y=1):
        # Sprawdzamy, czy y=0.
        if y == 0 and x != 0:
            raise ValueError('y nie moze byc rowne 0')
        self.x = x
        self.y = y

    def __str__(self):         # zwraca "x/y" lub "x" dla y=1
        if self.y == 1:
            return "%d" % self.x
        return "%d/%d" % (self.x, self.y)
    
    def __repr__(self):        # zwraca "Frac(x, y)"
        return "Frac(%d, %d)" % (self.x, self.y)
    
    def __cmp__(self, other):  # porownywanie
        if float(self) > float(other):
            return 1
        elif float(self) == float(other):
            return 0
        else:
            return -1

    def __add__(self, other):  # frac1+frac2, frac+int
        frac2 = other
        if not isinstance(other, Frac):
            if isinstance(other, (int, long, float)):
                frac2 = Frac(other, 1)
            else:
                raise ValueError('Given argument is not a int, long or float')

        frac = Frac(self.x * frac2.y + frac2.x * self.y, self.y * frac2.y)
        gcd = fractions.gcd(frac.x, frac.y)
        frac.x /= gcd
        frac.y /= gcd
        return frac
    
    __radd__ = __add__              # int+frac

    def __sub__(self, other):  # frac1-frac2, frac-int
        frac2 = other
        if not isinstance(other, Frac):
            if isinstance(other, (int, long, float)):
                frac2 = Frac(other, 1)
            else:
                raise ValueError('Given argument is not a int, long or float')

        frac = Frac(self.x * frac2.y - frac2.x * self.y, self.y * frac2.y)
        gcd = fractions.gcd(frac.x, frac.y)
        frac.x /= gcd
        frac.y /= gcd
        return frac
    
    def __rsub__(self, other):      # int-frac
        # tutaj self jest frac, a other jest int!
        return Frac(self.y * other - self.x, self.y)

    def __mul__(self, other):  # frac1*frac2, frac*int
        frac2 = other
        if not isinstance(other, Frac):
            if isinstance(other, (int, long, float)):
                frac2 = Frac(other, 1)
            else:
                raise ValueError('Given argument is not a int, long or float')

        result = Frac(self.x * frac2.x, self.y * frac2.y)
        gcd = fractions.gcd(result.x, result.y)
        result.x /= gcd
        result.y /= gcd
        return result 

    __rmul__ = __mul__              # int*frac

    def __div__(self, other):  # frac1/frac2, frac/int
        if not isinstance(other, Frac):
            if isinstance(other, (int, long, float)):
                return self * Frac(1, other)
            else:
                raise ValueError('Given argument is not a int, long or float')
        return self * (~other)

    def __rdiv__(self, other): # int/frac
        return other * self

    # operatory jednoargumentowe
    def __pos__(self):  # +frac = (+1)*frac
        return self

    def __neg__(self):         # -frac = (-1)*frac
        return (-1) * self
    
    def __invert__(self):      # odwrotnosc: ~frac
        return Frac(self.y, self.x)
    
    def __float__(self):       # float(frac)
        return self.x / float(self.y)