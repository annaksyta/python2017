import unittest
from fracs import *

class TestFrac(unittest.TestCase):
	
	def setUp(self):
		self.frac1 = Frac(1, 2)
		self.frac2 = Frac(2, 1)
	
	def test_init(self):
		with self.assertRaises(ValueError):
			frac = Frac(1, 0)
		try:
			frac = Frac(0, 0)
		except ValueError:
			self.fail("Frac(0, 0) raises ValueError")
	
	def test_str(self):
		self.assertEquals("1/2", self.frac1.__str__())
		self.assertEquals("2", self.frac2.__str__())
	
	def test_repr(self):
		self.assertEquals("Frac(1, 2)", self.frac1.__repr__())
	
	def test_float(self):
		self.assertEquals(0.5, float(self.frac1))
	
	def test_cmp(self):
		self.assertTrue(self.frac1 < self.frac2)
		self.assertTrue(self.frac2 > self.frac1)
		self.assertTrue(self.frac2 == Frac(2, 1))
	
	def test_add(self):
		self.assertEquals(Frac(5, 2), self.frac1 + self.frac2)
		self.assertEquals(Frac(1, 1), self.frac1 + self.frac1)
		self.assertEquals(Frac(1, 2), Frac(1, 3) + Frac(1, 6))
		self.assertEquals(Frac(5, 2), self.frac1 + 2)
		self.assertEquals(Frac(5, 2), 2 + self.frac1)
		with self.assertRaises(ValueError):
			self.frac1 + "as"
	
	def test_mul(self):
		self.assertEquals(Frac(1, 4), self.frac1 * self.frac1)
		self.assertEquals(Frac(1, 1), self.frac1 * self.frac2)
		self.assertEquals(Frac(5, 2), self.frac1 * 5)
		self.assertEquals(Frac(5, 2), 5 * self.frac1)
		with self.assertRaises(ValueError):
			self.frac1 * "as"
	
	def test_neg(self):
		self.assertEquals(Frac(-1, 2), -self.frac1)
		self.assertEquals(Frac(1, 2), -Frac(-1, 2))
	
	def test_invert(self):
		self.assertEquals(Frac(2, 1), ~self.frac1)
		self.assertEquals(Frac(1, 2), ~self.frac2)

	def test_post(self):
		self.assertEquals(Frac(1, 2), +self.frac1)
	
	def test_div(self):
		self.assertEquals(Frac(3, 4), self.frac1 / Frac(2, 3))
		self.assertEquals(Frac(1, 4), self.frac1 / 2)
		self.assertEquals(Frac(3, 2), 3 / self.frac1)
		self.assertEquals(Frac(1, 1), 2 / self.frac1)
		with self.assertRaises(ValueError):
			self.frac1 / "as"
	
	def test_sub(self):
		self.assertEquals(Frac(3, 2), 2 - self.frac1)
		self.assertEquals(Frac(-3, 2), self.frac1 - 2)
		self.assertEquals(Frac(3, 5), self.frac2 - Frac(7, 5))
		self.assertEquals(Frac(1, 4), self.frac1 - Frac(1, 4))
		with self.assertRaises(ValueError):
			self.frac1 - "as"
	
	def tearDown(self):
		pass

if __name__ == "__main__":
	unittest.main()