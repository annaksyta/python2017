import sys, os, math
dir_path = os.path.dirname(os.path.realpath(__file__))
dir_path = dir_path[:-1]
dir_path += "6"
sys.path.insert(0, dir_path)
from points import Point

class Circle:
    """Klasa reprezentujaca okregi na plaszczyznie."""

    def __init__(self, x=0, y=0, radius=1):
        if radius < 0:
            raise ValueError("promien ujemny")
        self.pt = Point(x, y)
        self.radius = radius

    def __repr__(self):       # "Circle(x, y, radius)"
        return "Circle(%g, %g, %g)" % (float(self.pt.x), float(self.pt.y), float(self.radius))

    def __eq__(self, other):
        return self.pt == other.pt and self.radius == other.radius

    def __ne__(self, other):
        return not self == other

    def area(self):           # pole powierzchni
        return math.pi * pow(self.radius, 2)

    def move(self, x, y):     # przesuniecie o (x, y)
        if not isinstance(x, (int, long, float)) or not isinstance(y, (int, long, float)):
            raise ValueError("One of given arguments is not a int, long or float")
        self.pt.x += x
        self.pt.y += y

    def cover(self, other):   # okrag pokrywajacy oba
        highY = 0
        lowY = 0
        leftX = 0
        rightX = 0
        if not isinstance(other, Circle):
            raise ValueError('Argument is not a Circle instance')

        if (self.pt.y + self.radius) > (other.pt.y + other.radius):
            highY = self.pt.y + self.radius
        else:
            highY = other.pt.y + other.radius
        if (self.pt.y - self.radius) < (other.pt.y - other.radius):
            lowY = self.pt.y - self.radius
        else:
            lowY = other.pt.y - other.radius
        if (self.pt.x + self.radius) > (other.pt.x + other.radius):
            rightX = self.pt.x + self.radius
        else:
            rightX = other.pt.x + other.radius
        if (self.pt.x - self.radius) < (other.pt.x - other.radius):
            leftX = self.pt.x - self.radius
        else:
            leftX = other.pt.x - other.radius

        dHorizontal = math.sqrt(pow(rightX - leftX, 2))
        dVertical = math.sqrt(pow(lowY - highY, 2))
        centerX = leftX + (dHorizontal / 2)
        centerY = lowY + (dVertical / 2)
        radius = 0
        if dVertical > dHorizontal:
            radius = dVertical / 2
        else:
            radius = dHorizontal / 2
        return Circle(centerX, centerY, radius)

