#!/usr/bin/python
#Anna Ksyta

#Mamy dany napis wielowierszowy line. 
#Podac sposob obliczenia liczby wyrazow w napisie. 
#Przez wyraz rozumiemy ciag "czarnych" znakow, 
#oddzielony od innych wyrazow bialymi znakami (spacja, tabulacja, newline).

line = 'abc\tdef xyz\n\nabc'
print len(line.split())