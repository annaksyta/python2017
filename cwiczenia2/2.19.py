#!/usr/bin/python
#Anna Ksyta

#Na liscie L mamy liczby jedno-, dwu- i trzycyfrowe dodatnie. 
#Chcemy zbudowac napis z trzycyfrowych blokow, 
#gdzie liczby jedno- i dwucyfrowe beda mialy blok dopelniony zerami, np. 007, 024. Wskazowka: str.zfill().

import sys

L = (1, 10, 214, 12, 1, 21, 32, 21, 2, 5, 121, 234, 523)
output = ''
for number in L:
	output += repr(number).zfill(3)
print output