#!/usr/bin/python
#Anna Ksyta

#Znalezc liczbe cyfr zero w duzej liczbie calkowitej. Wskazowka: zamienic liczbe na napis.

number = 2104000241042142010421004210000
print 'Liczba zer w %d = %d' % (number, repr(number).count('0'))