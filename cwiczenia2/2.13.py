#!/usr/bin/python
#Anna Ksyta

#Znalezc laczna dlugosc wyrazow w napisie line. Wskazowka: mozna skorzystac z funkcji sum().

line = 'przykladowy string wymyslony na poczekaniu'

wordLengths = map(len, line.split())
print sum(wordLengths)