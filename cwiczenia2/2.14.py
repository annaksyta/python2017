#!/usr/bin/python
#Anna Ksyta

#Znalezc: (a) najdluzszy wyraz, (b) dlugosc najdluzszego wyrazu w napisie line.

line = 'przykladowy string wymyslony na poczekaniu'
print 'Najdluzszy wyraz to: \'%s\' ma dlugosc = %d' % (max(line.split(), key=len), len(max(line.split(), key=len)))