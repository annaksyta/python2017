#!/usr/bin/python
#Anna Ksyta

#Posortowac wyrazy z napisu line raz alfabetycznie, a raz pod wzgledem dlugosci. 
#Wskazowka: funkcja wbudowana sorted().

line = 'przykladowy wyraz do posortowania, alfabetycznie, pod wzgledem dlugosci'
print 'Alfabetycznie:'
print sorted(line.split())
print 'Wzgledem dlugosci:'
print sorted(line.split(), key=len)