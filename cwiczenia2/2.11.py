#!/usr/bin/python
#Anna Ksyta

#Podac sposob wyswietlania napisu word tak, aby jego znaki byly rozdzielone znakiem podkreslenia.

import sys

word = 'word'
for index, letter in enumerate(word):
	if index != len(word)-1:
		sys.stdout.write(letter + '_')
	else:
		sys.stdout.write(letter)
print