#!/usr/bin/python
#Anna Ksyta

#Na liscie L znajduja sie liczby calkowite dodatnie. Stworzyc napis bedacy ciagiem cyfr kolejnych liczb z listy L.

import sys

L = (1, 331, 124, 655, 384, 42, 43, 24)
output = ''
for number in L:
	output += repr(abs(number))
print output

