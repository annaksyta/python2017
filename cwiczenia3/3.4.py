#!/usr/bin/python
# Anna Ksyta

#Napisac program pobierajccy w petli od uzytkownika liczbe rzeczywista x i wypisujacy pare x i trzecia potege x. 
#Zatrzymanie programu nastepuje po wpisaniu z klawiatury stop. Jezeli uzytkownik wpisze napis zamiast liczby, 
#to program ma wypisac komunikat o bledzie i kontynuowac prace.


while True:
	user_input = raw_input()
	try:
		value = float(user_input)
		print value, pow(value, 3)
	except ValueError:
		if user_input == 'stop':
			break
		else:
			print 'Podana wartosc nie jest liczba'