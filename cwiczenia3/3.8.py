#!/usr/bin/python
# Anna Ksyta

#Dla dwoch sekwencji znalezc: 
#(a) liste elementow wystepujacych w obu sekwencjach (bez powtorzen), 
#(b) liste wszystkich elementow z obu sekwencji (bez powtorzen).



sequence1 = [1, 2, 1, 342, 21, 21, 31]
sequence2 = [1, 2, 1, 54, 4, 34, 2]

outputA = []
outputB = []
for i in sequence1:
	if i not in outputB:
		outputB.append(i)
	if i in sequence2 and i not in outputA:
		outputA.append(i)
for i in sequence2:
	if i not in outputB:
		outputB.append(i)
		
print 'Output a): ', outputA
print 'Output b): ', outputB