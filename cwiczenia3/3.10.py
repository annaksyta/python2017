#!/usr/bin/python
# Anna Ksyta

#Stworzyc slownik tlumaczacy liczby zapisane w systemie rzymskim (z literami I, V, X, L, C, D, M) 
#na liczby arabskie (podac kilka sposobow tworzenia takiego slownika). 
#Mile widziany kod tlumaczacy cala liczbe [funkcja roman2int()].


import sys

dictionary = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}

# inny sposob stworzenia slownika

dictionary2 = {}
dictionary2["I"] = 1;
dictionary2["V"] = 5;
dictionary2["X"] = 10;
dictionary2["L"] = 50;
dictionary2["C"] = 100;
dictionary2["D"] = 500;
dictionary2["M"] = 1000;

# nastepny sposob stworzenia slownika
digits = ["I", "V", "X", "L", "C", "D", "M"]
numbers = [1, 5, 10, 50, 100, 500, 1000]
dictionary3 = dict(zip(digits, numbers))

while True:
	value = raw_input('Wpisz rzymska liczbe lub stop, aby zakonczyc\n')
	if value == 'stop':
		sys.exit()
	else:
		try:
			print value, '=', dictionary[value]
		except:
			print 'nie jest obslugiwana liczba rzymska'

# solution 2:
#digits = ["I", "V", "X", "L", "C", "D", "M"]
#numbers = [1, 5, 10, 50, 100, 500, 1000]
#while True:
#	value = raw_input('Wpisz rzymska liczbe lub stop, aby zakonczyc\n')
#	if value == 'stop':
#		sys.exit()
#	else:
#		try:
#			print value, '=', numbers[digits.index(value)]
#		except:
#			print value, '= nie jest obslugiwana liczba rzymska'

# solution 3:
#while True:
#	value = raw_input('Wpisz rzymska liczbe lub stop, aby zakonczyc\n')
#	if value == 'stop':
#		sys.exit()
#	elif value == 'I':
#		print value, '=', 1
#	elif value == 'V':
#		print value, '=', 5
#	elif value == 'X':
#		print value, '=', 10
#	elif value == 'L':
#		print value, '=', 50
#	elif value == 'C':
#		print value, '=', 100
#	elif value == 'D':
#		print value, '=', 500
#	elif value == 'M':
#		print value, '=', 1000
#	else:
#		print value, '= nie jest obslugiwana liczba rzymska