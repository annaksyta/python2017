from os.path import expanduser, exists, abspath

class Settings:
    MAIN_FILE = expanduser("~") + "/db_settings.ini"
    DEFAULT_DB_DIRECTORY = expanduser("~") + "/dbs"
    
    def __init__(self):
        db_dir = self._getDatabaseDirectories()
        if db_dir is None:
            self._createSettingsFile()
            self.dbs_directory = Settings.DEFAULT_DB_DIRECTORY
        else:
            self.dbs_directory = db_dir

    """
    	Creates new settings file
    """
    def _createSettingsFile(self):
        with open(Settings.MAIN_FILE, "w") as fp:
            fp.write(Settings.DEFAULT_DB_DIRECTORY)

    """
    	Gets database directory from file. If file doesnt exists return None
    """
    def _getDatabaseDirectories(self):
        try:
            with open(Settings.MAIN_FILE, "r") as fp:
                return fp.read()
        except IOError:
            return None

    """
    	Gets path to databases directory.
    """
    def getDbsDirectory(self):
        return self.dbs_directory

    """
    	Changes path to databases directory.
    """
    def changeDbsDirectory(self, new_dir):
        with open(Settings.MAIN_FILE, "w") as fp:
            fp.write(abspath(new_dir))
        self.dbs_directory = abspath(new_dir)

    """
    	Checks if given location is available.
    """
    def isLocationPossible(self, location):
        return exists(location)
