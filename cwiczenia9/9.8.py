def bst_max(top):
    if top is None:
        raise ValueError("Drzewo jest puste!")
    else:
        while top.right is not None:
            top = top.right
        return top


def bst_min(top):
    if top is None:
        raise ValueError("Drzewo jest puste!")
    else:
        while top.left is not None:
            top = top.left
        return top