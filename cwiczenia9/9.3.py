class Node:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)


def find_max(node):
    max_node = node;
    tmp_node = node;
    while (tmp_node != None):
        if (tmp_node.data > max_node.data):
            max_node = tmp_node
        tmp_node = tmp_node.next
    return max_node


def find_min(node):
    min_node = node;
    tmp_node = node;
    while (tmp_node != None):
        if (tmp_node.data < min_node.data):
            min_node = tmp_node
        tmp_node = tmp_node.next
    return min_node