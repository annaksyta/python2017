import generator
import os

def dump(L, dataFile):
    f = open(dataFile, 'w')
    a = 0;
    for i in L:
        f.write(str(a) + ' ' + str(i) + '\n')
        a += 1


def merge(L, left, middle, right):
    T = []
    left1 = left
    right1 = middle
    left2 = middle + 1
    right2 = right
    while left1 <= right1 and left2 <= right2:
        if L[left1] < L[left2]:
            T.append(L[left1])
            left1 = left1 + 1
        else:
            T.append(L[left2])
            left2 = left2 + 1
    while left1 <= right1:
        T.append(L[left1])
        left1 = left1 + 1
    while left2 <= right2:
        T.append(L[left2])
        left2 = left2 + 1
    for i in range(left, right + 1):
        L[i] = T[i - left]


def mergesort(L, left, right):
    if left < right:
        global v
        middle = (left + right) / 2
        mergesort(L, left, middle)
        mergesort(L, middle + 1, right)
        merge(L, left, middle, right)

        if v % 1 == 0:
            dump(L, 'plik' + str(v) + '.dat')
            os.system(
                "gnuplot -e \"dataFile='plik" + str(v) + ".dat'; imageFile='wynik" + str(v) + ".eps'\" sortples.gnu")
        v += 1

if __name__ == "__main__":
    numbers = generator.List.random(30)

    v = 0

    mergesort(numbers, 0, len(numbers) - 1)