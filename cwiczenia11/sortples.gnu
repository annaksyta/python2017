set term postscript eps enhanced color 22
set output imageFile

set title "Sortowanie X"
set xlabel "numer pozycji"              # opis osi x
set ylabel "liczba na pozycji"          # opis osi y
unset key                               # bez legendy

plot dataFile using 1:2 with points pt 5