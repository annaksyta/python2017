import random
import math


class List():
    @staticmethod
    def notRandom(n):
        return range(n)

    @staticmethod
    def almostRandom(n):
        l = List.notRandom(n)
        lAR = []
        for i in range(0, n, 5):
            slice = l[i:i + 5]
            random.shuffle(slice)
            lAR.extend(slice)
        return lAR

    @staticmethod
    def almostRandomReversed(n):
        lAR = List.almostRandom(n)
        lAR.reverse()
        return lAR

    @staticmethod
    def random(n):
        l = range(n)
        random.shuffle(l)
        return l

    @staticmethod
    def gaussDistributed(n):
        l = []
        for i in range(n):
            l.append(int(random.gauss(0, math.sqrt(n))))
        return l

    @staticmethod
    def randomDuplicates(n):
        l = []
        for i in range(n):
            l.append(random.randint(0, math.sqrt(n)))
        return l