#!/usr/bin/python
# Anna Ksyta

from fracs import *
import unittest

class TestFractions(unittest.TestCase):

    def setUp(self):
        self.zero = [0, 1]

    def test_add_frac(self):
        self.assertEqual(add_frac([1, 2], [1, 3]), [5, 6])
        self.assertEqual(add_frac([3, 4], [1, 5]), [19, 20])
        self.assertEqual(add_frac([10, 11], [1, 12]), [131, 132])
        self.assertEqual(add_frac([1, 12], [2, 3]), [3, 4])
        self.assertEqual(add_frac(self.zero, [1, 2]), [1, 2])

    def test_sub_frac(self):
        self.assertEqual(sub_frac([1, 2], [1, 3]), [1, 6])
        self.assertEqual(sub_frac([3, 4], [1, 5]), [11, 20])
        self.assertEqual(sub_frac([1, 2], [1, 1]), [-1, 2])

    def test_mul_frac(self):
        self.assertEqual(mul_frac([1, 2], [1, 2]), [1, 4])
        self.assertEqual(mul_frac([3, 4], [1, 3]), [1, 4])
        self.assertEqual(mul_frac([1, 12], [1, 11]), [1, 132])
        self.assertEqual(mul_frac(self.zero, [1, 2]), [0, 1])

    def test_div_frac(self):
        self.assertEqual(div_frac([1, 2], [1, 2]), [1, 1])
        self.assertEqual(div_frac([3, 4], [1, 2]), [3, 2])
        self.assertEqual(div_frac([1, 12], [1, 11]), [11, 12])
        self.assertEqual(div_frac(self.zero, [1, 2]), [0, 1])

    def test_is_positive(self):
        self.assertEqual(is_positive([1, 2]), True)
        self.assertEqual(is_positive([-1, 2]), False)
        self.assertEqual(is_positive(self.zero), False)

    def test_is_zero(self):
        self.assertEqual(is_zero(self.zero), True)
        self.assertEqual(is_zero([1, 2]), False)
        self.assertEqual(is_zero([-1, 2]), False)

    def test_cmp_frac(self):
        self.assertEqual(cmp_frac(self.zero, self.zero), 0)
        self.assertEqual(cmp_frac(self.zero, [1, 2]), 1)
        self.assertEqual(cmp_frac(self.zero, [-1, 2]), -1)

    def test_frac2float(self):
        self.assertEqual(frac2float([1, 2]), 0.5)
        self.assertEqual(frac2float([0, 1]), 0)
        self.assertEqual(frac2float([1, 4]), 0.25)
        self.assertEqual(frac2float([-1, 4]), -0.25)

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()