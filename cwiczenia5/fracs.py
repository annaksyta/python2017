#!/usr/bin/python
# Anna Ksyta

import fractions

# frac1 + frac2
def add_frac(frac1, frac2):
	result = []
	result.append(frac1[0] * frac2[1] + frac2[0] * frac1[1])
	result.append(frac1[1] * frac2[1])
	gcd = fractions.gcd(result[0], result[1])
	result[0] /= gcd
	result[1] /= gcd
	return result

# frac1 - frac2
def sub_frac(frac1, frac2):
	result = []
	result.append(frac1[0] * frac2[1] - frac2[0] * frac1[1])
	result.append(frac1[1] * frac2[1])
	gcd = fractions.gcd(result[0], result[1])
	result[0] /= gcd
	result[1] /= gcd
	return result

# frac1 * frac2
def mul_frac(frac1, frac2):
	result = [frac1[0] * frac2[0], frac1[1] * frac2[1]]
	gcd = fractions.gcd(result[0], result[1])
	result[0] /= gcd
	result[1] /= gcd
	return result

# frac1 / frac2
def div_frac(frac1, frac2):
	return mul_frac(frac1, frac2[::-1])

# bool, czy dodatni
def is_positive(frac):
	if frac[0] > 0 and frac[1] > 0:
		return True
	return False

# bool, typu [0, x]
def is_zero(frac):
	if frac[0] == 0:
		return True
	return False              

# -1 | 0 | +1
def cmp_frac(frac1, frac2):
	if frac1[0] == frac2[0] and frac1[1] == frac2[1]:
		return 0
	elif frac2float(frac1) < frac2float(frac2):
		return 1
	else:
		return -1
	
# konwersja do float
def frac2float(frac):
	return frac[0] / float(frac[1])