#!/usr/bin/python
# Anna Ksyta

def factorial(n):
	result = 1
	for i in range(1, n+1):
		result *= i
	return result
	
def fibonacci(n):
	result = 0
	y = 1
	x = 1
	for i in range(n+1):
		result = y
		y = x
		x = result + y
	return result