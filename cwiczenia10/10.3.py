class Stack:
    def __init__(self, size=10):
        self.items = size * [None]
        self.exist = size * [None]

        self.n = 0
        self.size = size

    def is_empty(self):
        return self.n == 0

    def is_full(self):
        return self.size == self.n

    def push(self, data):
        if data > self.size - 1:
            raise Exception("Wartosc poza przedzialem")
        if self.is_full():
            raise Exception("Stos jest pelny")
        if self.exist[data] != 1:
            self.items[self.n] = data
            self.n = self.n + 1
            self.exist[data] = 1

    def pop(self):
        if self.is_empty():
            raise Exception("Stos jest pusty")
        self.n = self.n - 1
        data = self.items[self.n]
        self.items[self.n] = None
        self.exist[data] = 0
        return data

if __name__ == "__main__":
    s = Stack(5)

    s.push(3)
    s.push(2)
    s.push(4)
    s.push(3)
    s.push(2)

    print s.pop()
    print s.pop()
    print s.pop()