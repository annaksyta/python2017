import random


class RandomQueue:
    def __init__(self):
        self.items = []

    def insert(self, item):
        self.items.append(item)

    def remove(self):
        lastIndex = len(self.items) - 1
        randomIndex = random.randint(0, lastIndex)
        self.items[randomIndex], self.items[lastIndex] = self.items[lastIndex], self.items[randomIndex]
        return self.items.pop(lastIndex)

    def is_empty(self):
        return not self.items

if __name__ == "__main__":
    rq = RandomQueue()

    rq.insert(1)
    rq.insert(2)
    rq.insert(-3)
    rq.insert(4)

    print rq.remove()
    print rq.remove()
    print rq.remove()
    print rq.remove()